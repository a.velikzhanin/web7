function updatePrice() {
  
  let s = document.getElementsByName("prodType");
  let select = s[0];
  let price = 0;
  
  
  let prices = getPrices();
  let priceIndex = parseInt(select.value) - 1;
  if (priceIndex >= 0) {
    price = prices.prodTypes[priceIndex];
  }
  
  // Скрываем или показываем радиокнопки.
  let radioDiv = document.getElementById("radios");
  radioDiv.style.display = (select.value == "2" ? "block" : "none");
  
  // Смотрим какая товарная опция выбрана.
  let radios = document.getElementsByName("prodOptions");
  radios.forEach(function(radio) {
    if (radio.checked) {
      let optionPrice = prices.prodOptions[radio.value];
      if (optionPrice !== undefined) {
        price += optionPrice;
      }
    }
  });

  // Скрываем или показываем чекбоксы.
  let checkDiv = document.getElementById("checkboxes");

  if (select.value == "3") checkDiv.style.display ="block";
  else checkDiv.style.display="none";
  if (select.value == "1") 
  {
    checkDiv.style.display="none";
    radioDiv.style.display="none";
  }
  
  
  // Смотрим какие товарные свойства выбраны.
  let checkboxes = document.querySelectorAll("#checkboxes input");
  checkboxes.forEach(function(checkbox) {
    if (checkbox.checked) {
      let propPrice = prices.prodProperties[checkbox.name];
      if (propPrice !== undefined) {
        price += propPrice;
      }
    }
  });
  //месяцев
  var month = document.getElementsByName("month1");
  if (month[0].value.match(/^[0-9]+$/))
    
  {
    var prodPrice = document.getElementById("prodPrice");
  prodPrice.innerHTML = price*month[0].value + " рублей";
  }
    else 
    alert("Пожалуйста, вводите числа с целыми значениями");



  
}

function getPrices() {
  return {
    prodTypes: [75*1000, 0, 1000*1000],
    prodOptions: {
      option1: 150*1000,
      option2: 250*1000,
      option3: 500*1000,
    },
    prodProperties: {
      prop1: 100*1000,
      prop2: 100*1000,
    }
  };
}

window.addEventListener("DOMContentLoaded", function () {
//
//          SLIDER
//

$(".slider").slick({
  dots: true,
  infinite: false,
  arrows: true,
  speed: 1*1000,
  slidesToShow: 4,
  slidesToScroll: 4,
  // !!! стрелки почему-то не отображаются на белом фоне, но кликаются. 
  //если задать серый бек, то они будут видны.
  
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    
  ]
});


  // Скрываем радиокнопки.
  let radioDiv = document.getElementById("radios");
  radioDiv.style.display = "none";
  let checkDiv = document.getElementById("checkboxes");
  checkDiv.style.display = "none";
  // Находим select по имени в DOM.
  let s = document.getElementsByName("prodType");
  let select = s[0];
  // Назначаем обработчик на изменение select.
  select.addEventListener("change", function (event) {
      updatePrice();
    });
  
  
  // Назначаем обработчик радиокнопок.  
  let radios = document.getElementsByName("prodOptions");
  radios.forEach(function(radio) {
    radio.addEventListener("change", function(event) {
      let r = event.target;
      console.log(r.value);
      updatePrice();
    });
  });

    // Назначаем обработчик радиокнопок.  
  let checkboxes = document.querySelectorAll("#checkboxes input");
  checkboxes.forEach(function(checkbox) {
    checkbox.addEventListener("change", function(event) {
      let c = event.target;
      console.log(c.name);
      console.log(c.value);
      updatePrice();
    });
  });
// обработчик количества месяцев + подсчет

let m = document.getElementsByName("month1");
let month = m[0];

month.addEventListener("change", function(event) {
  let target2 = event.target;
  console.log(target2.value);
  updatePrice();
});
});






